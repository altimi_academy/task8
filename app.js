/**
*Write a JavaScript function to determine whether a given year
*is a leap year in the Gregorian calendar
*
*ALGORITHM:
*if (year is not divisible by 4) then (it is a common year)
*else if (year is not divisible by 100) then (it is a leap year)
*else if (year is not divisible by 400) then (it is a common year)
*else (it is a leap year)
*
*@return {boolean}
*/
function isLeap(year) {
	if (year%4 !== 0) {
		console.log('not dividible by 4 - common year');
		return false;
	}
	else if (year%100 === 0) {
		if (year%400 === 0) {
			console.log('dividible by 400 - leap year');
			return true;
		}
		console.log('dividible by 100 - common year');
		return false;
	}
	else {
		console.log('dividible by 4 and not by 100 - leap year');
		return true;
	}
}